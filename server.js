'use strict';

const express = require('express');
const path = require('path')

// Constantes
const PORT = 8081;

// App
const app = express();
app.get('/', function(req, res) {
  // res.send("Bienvenido terrícola\n")
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.listen(PORT);
console.log("Express funcionando en el puerto " + PORT);
