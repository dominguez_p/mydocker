FROM node:boron

# Crear directorio de la app
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Instalar dependencias
COPY package.json /usr/src/app
RUN npm install

# Empaquetar código
COPY . /usr/src/app

# Exponer el puerto del docker para que se pueda acceder desde fuera
EXPOSE 8081

# Arrancar el servicio npm
CMD [ "npm", "start" ]
